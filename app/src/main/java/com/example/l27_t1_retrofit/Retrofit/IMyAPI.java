package com.example.l27_t1_retrofit.Retrofit;


import com.example.l27_t1_retrofit.Model.Post;

import java.util.List;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface IMyAPI  {

    @GET("todos")
    Observable<List<Post>> getPosts();

}
