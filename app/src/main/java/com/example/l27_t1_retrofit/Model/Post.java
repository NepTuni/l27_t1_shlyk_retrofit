package com.example.l27_t1_retrofit.Model;

public class Post {
    public int userId;
    public int id;
    public String title;
    public boolean completed;

    public Post(){
    }

    public Post(int userId, int id, String title, boolean completed) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.completed = completed;
    }
}
