package com.example.l27_t1_retrofit.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.l27_t1_retrofit.Model.Post;
import com.example.l27_t1_retrofit.R;

import java.util.List;
import java.util.Random;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PostAdapter extends RecyclerView.Adapter<PostViewHolder> {

    Context context;
    List<Post> postList;
    String doneText;
    String todoText;
    private static final String USER = "Assignee: User";

    public PostAdapter(Context context, List<Post> postList) {
        this.context = context;
        this.postList = postList;
        this.doneText = context.getString(R.string.done);
        this.todoText = context.getString(R.string.todo);
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_task, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PostViewHolder holder, int position) {
        holder.txt_userId.setText(USER + String.valueOf(postList.get(position).userId));
        holder.txt_id.setText(String.valueOf(postList.get(position).id));
        final Drawable d = holder.txt_id.getContext().getResources().getDrawable(R.drawable.circle_number);
        d.setTint(getRandomColor());
        holder.txt_id.setBackground(d);
        holder.txt_title.setText(String.valueOf(postList.get(position).title));
        checkStatus(holder, postList.get(position).completed);
        holder.mChip.setTextSize(18);
        holder.mChip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mChip.getText().toString().equals("Done")) {
                    holder.mChip.setChipBackgroundColorResource(R.color.colorTodo);
                    holder.mChip.setText(todoText);
                } else {
                    holder.mChip.setChipBackgroundColorResource(R.color.colorDone);
                    holder.mChip.setText(doneText);
                }
            }
        });

    }

    public void checkStatus(PostViewHolder postViewHolder, boolean status){
        if(status) {
            postViewHolder.mChip.setChipBackgroundColorResource(R.color.colorTodo);
            postViewHolder.mChip.setText(todoText);
        }
            else{
                postViewHolder.mChip.setChipBackgroundColorResource(R.color.colorDone);
                postViewHolder.mChip.setText(doneText);
            }

    }


    @Override
    public int getItemCount() {
        return postList.size();
    }

    private int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

}
