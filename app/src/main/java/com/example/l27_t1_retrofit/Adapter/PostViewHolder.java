package com.example.l27_t1_retrofit.Adapter;

import android.view.View;
import android.widget.TextView;

import com.example.l27_t1_retrofit.R;
import com.google.android.material.chip.Chip;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PostViewHolder extends RecyclerView.ViewHolder {
    TextView txt_id, txt_userId, txt_title;
    Chip mChip;
    public PostViewHolder(@NonNull View itemView) {
        super(itemView);

        txt_id = itemView.findViewById(R.id.imageNumber);
        txt_title = itemView.findViewById(R.id.textTitle);
        txt_userId = itemView.findViewById(R.id.textUserId);
        mChip = itemView.findViewById(R.id.chipView);
    }
}
